# coding=UTF-8

import logging
log = logging.getLogger(__name__)

from django.core.management.base import BaseCommand
from map_server.models import LayerDataset


class Command(BaseCommand):
    help = 'Generates all feeds set up as LayerDatasetFromSourceCode objects'

    def handle(self, *args, **options):
        datasets = LayerDataset.objects.filter(source_type='source_code')

        for dataset in datasets:
            dataset.generate_feed()
