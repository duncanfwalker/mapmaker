# coding=UTF-8

import logging
log = logging.getLogger(__name__)

import os
import json
from django.conf import settings
from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from map_server.utils import extractPostcode, geocodePostcode


class Command(BaseCommand):
    help = 'Grabs events feed, geocodes, and writes to feeds dir'

    def handle(self, *args, **options):
        feed_filename = 'upcoming-events.json'
        event_feed_url = 'http://www.foe.co.uk/rss/upcoming_events.xml'

        event_feed = requests.get(event_feed_url, timeout=60)
        soup = BeautifulSoup(event_feed.text, 'xml')
        events = []

        for item in soup.findAll('node'):
            try:
                item = BeautifulSoup(str(item), 'xml')
                if item.eventType.get_text() != "Bee event":
                    continue
                event = {'location': item.location.get_text(), 'title': item.title.get_text(),
                         'description': item.description.get_text(), 'link': item.link.get_text(),
                         'date': item.startdate.get_text(), 'time': item.starttime.get_text()}

                event['postcode'] = extractPostcode(event['location'])
                geocode_result = geocodePostcode(event['postcode'])
                event = dict(event.items() + geocode_result.items())
                events.append(event)
            except Exception as e:
                log.error('Could not process event feed item: %s' % e)

            full_feed_path = os.path.join(getattr(settings, "FEED_OUTPUT_DIR"), feed_filename)
            with open(full_feed_path, "w") as f:
                f.write(json.dumps(events))
                log.info('%s written to disk' % full_feed_path)