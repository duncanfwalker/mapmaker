#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been automatically generated.
# Instead of changing it, create a file called import_helper.py
# which this script has hooks to.
#
# On that file, don't forget to add the necessary Django imports
# and take a look at how locate_object() and save_or_locate()
# are implemented here and expected to behave.
#
# This file was generated with the following command:
# /Users/Patrick/Documents/FoE Map/server/manage.py dumpscript auth.user
#
# to restore it, run
# manage.py runscript module_name.this_script_name
#
# example: if manage.py is at ./manage.py
# and the script is at ./some_folder/some_script.py
# you must make sure ./some_folder/__init__.py exists
# and run  ./manage.py runscript some_folder.some_script


IMPORT_HELPER_AVAILABLE = False
try:
    import import_helper
    IMPORT_HELPER_AVAILABLE = True
except ImportError:
    pass

import datetime
from decimal import Decimal
from django.contrib.contenttypes.models import ContentType

def run():
    #initial imports

    def locate_object(original_class, original_pk_name, the_class, pk_name, pk_value, obj_content):
        #You may change this function to do specific lookup for specific objects
        #
        #original_class class of the django orm's object that needs to be located
        #original_pk_name the primary key of original_class
        #the_class      parent class of original_class which contains obj_content
        #pk_name        the primary key of original_class
        #pk_value       value of the primary_key
        #obj_content    content of the object which was not exported.
        #
        #you should use obj_content to locate the object on the target db
        #
        #and example where original_class and the_class are different is
        #when original_class is Farmer and
        #the_class is Person. The table may refer to a Farmer but you will actually
        #need to locate Person in order to instantiate that Farmer
        #
        #example:
        #if the_class == SurveyResultFormat or the_class == SurveyType or the_class == SurveyState:
        #    pk_name="name"
        #    pk_value=obj_content[pk_name]
        #if the_class == StaffGroup:
        #    pk_value=8


        if IMPORT_HELPER_AVAILABLE and hasattr(import_helper, "locate_object"):
            return import_helper.locate_object(original_class, original_pk_name, the_class, pk_name, pk_value, obj_content)

        search_data = { pk_name: pk_value }
        the_obj =the_class.objects.get(**search_data)
        return the_obj

    def save_or_locate(the_obj):
        if IMPORT_HELPER_AVAILABLE and hasattr(import_helper, "save_or_locate"):
            the_obj = import_helper.save_or_locate(the_obj)
        else:
            the_obj.save()
        return the_obj



    #Processing model: User

    from django.contrib.auth.models import User

    if not User.objects.filter(username='admin'):

        auth_user_1 = User()
        auth_user_1.username = u'moderator'
        auth_user_1.first_name = u''
        auth_user_1.last_name = u''
        auth_user_1.email = u''
        auth_user_1.password = u''
        auth_user_1.is_staff = True
        auth_user_1.is_active = True
        auth_user_1.is_superuser = False
        auth_user_1.last_login = datetime.datetime(2013, 5, 24, 13, 31, 32)
        auth_user_1.date_joined = datetime.datetime(2013, 5, 24, 13, 31, 32)
        auth_user_1 = save_or_locate(auth_user_1)

        auth_user_2 = User()
        auth_user_2.username = u'admin'
        auth_user_2.first_name = u''
        auth_user_2.last_name = u''
        auth_user_2.email = u''
        auth_user_2.password = u''
        auth_user_2.is_staff = True
        auth_user_2.is_active = True
        auth_user_2.is_superuser = True
        auth_user_2.last_login = datetime.datetime(2013, 5, 24, 13, 33, 8, 405105)
        auth_user_2.date_joined = datetime.datetime(2013, 5, 24, 13, 32, 16)
        auth_user_2 = save_or_locate(auth_user_2)

        #Re-processing model: User